#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <set>
using std::set;
#include <iostream>
using std::cin;
using std::cout;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	string str1;
	string str2;
	int count = 1;

if(showcount == 0 && dupsonly == 0 && uniqonly == 0)
	{
		while(getline(cin,str1))
		{
			cout << str1 << '\n';
			getline(cin,str2);

			if(str1 != str2)
				cout << str2 << '\n';
			else
				str1.clear();
		}
	}


	if(showcount)
	{
		getline(cin,str1);
		while(getline(cin,str2))
		{
			if(str1 == str2)
			{
				count++;
			}

			if(str1 != str2)
			{
				cout << '\t' << count << " " << str1 << '\n';
				count = 1;
				str1.clear();
				str1 = str2;
			}
		}
		cout << '\t' << count << " " << str1 << '\n';
	}


	if(dupsonly)
	{
		getline(cin,str1);
		while(getline(cin,str2))
		{
			if(str1 == str2)
			{
				count++;
				while(getline(cin,str2))
				{
					if(str1 == str2)
					{
						count++;
						str1 = str2;
					}
					if(str1 != str2)
					{
						count = 1;
						cout << '\t' << str1 << '\n';
						break;
					}
				}
				str1 = str2;
			}

			else
				str1 = str2;
		}

		if(count > 1)
			cout << '\t' << str1 << '\n';
	}

/*
if(dupsonly = 1)
{
	getline(cin,str1);
	while(getline(cin,str2))
	{
		if(str1 == str2)
		{
			cout << str1 << '\n';
		}

		if(str1 != str2)
		{
			cout << str2 << '\n';
		}

		str1.clear();
		str1 = str2;
	}
}
*/
	if(uniqonly)
	{

	}

	return 0;
}
